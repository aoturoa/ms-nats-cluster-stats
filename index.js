'use strict';

const config = require('config');
const yves = require('yves');
yves.debugger().enable(config.get('debug.pattern'))
const debugApp = yves.debugger(config.get('debug.namespace'));
const debugService = yves.debugger(config.get('debug.namespace') + ':service');

const _ = require('lodash');
const os = require('os');
const pkg = require('./package.json');
const uuidv4 = require('uuid/v4');
const nats = require("nats");
const natsConn = nats.connect(config.get('nats.client'));
const url = require('url');
const request = require('request');

// Global instance.
let instance = {
  id: uuidv4(),
  cache: {},
};

function init() {
  debugApp('Start application');
  const endpoints = config.get('endpoints');
  const monitor = config.get('nats.monitor');
  for (var i in endpoints) {
    const endpoint = endpoints[i];
    endpoint.uri = url.format(Object.assign(
      config.get('nats.monitor'), endpoint
    ));
    endpoint.channel = config.get('channel').replace('[IDENTIFIER]', endpoint.identifier);
    debugApp('Polling endpoint: %y', endpoint);
    startPollingEndpoint(endpoint);
  }
}

function startPollingEndpoint(endpoint) {
  debugService('Requesting uri: %s', endpoint.uri);
  request(endpoint.uri, {json: true}, (error, response, body) => {
    if (error) {
      return console.log(error);
    }
    const message = formatMessage(body);
    // Update local cache.
    instance.cache[endpoint.identifier] = message;
    // Publish via NATS.
    debugService('Publishing in channel: %s', endpoint.channel);
    natsConn.publish(
      endpoint.channel,
      JSON.stringify(message)
    );
  });
  setTimeout(
    startPollingEndpoint,
    _.get(endpoint, 'interval', 60000),
    endpoint
  );
}

function formatMessage(payload) {
  return {
    payload: payload,
    meta: {
      hostname: os.hostname(),
    },
    system: {
      id: instance.id,
      timestamp: Date.now(),
      name: `"${pkg.name}"`,
    },
  }
}

init();