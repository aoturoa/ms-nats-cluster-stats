module.exports = {
  debug: {
    namespace: 'ms:nats:cluster:stats',
    pattern: process.env.hasOwnProperty('DEBUG') ? process.env.DEBUG : null,
  },
  nats: {
    client: {
      url: "nats://localhost:4222",
      waitOnFirstConnect: true,
      reconnect: true
    },
    monitor: {
      protocol: 'http',
      hostname: 'localhost',
      port: 8222
    }
  },
  channel: 'ms.nats.cluster.stats.[IDENTIFIER]',
  endpoints: [
    {
      identifier: 'varz',
      pathname: 'varz',
      interval: 30000
    },
    {
      identifier: 'connz',
      pathname: 'connz',
      interval: 30000
    },
    {
      identifier: 'routez',
      pathname: 'routez',
      interval: 30000
    },
    {
      identifier: 'subsz',
      pathname: 'subsz',
      interval: 30000
    },
  ]
};
